import {createServer} from "./components/express-server";
import setUpApi from "./http-routes";
import * as http from 'http';
import * as https from 'https';
import * as fs from 'fs';

const server = createServer();
setUpApi(server);

if(!process.env.IS_NOW) {
    const httpsOptions: https.ServerOptions = {
        key: fs.readFileSync('./.ignore/ssl/v.valt.me.key', 'utf-8'),
        cert: fs.readFileSync('./.ignore/ssl/v.valt.me.pem', 'utf-8')
    };
    const httpServer = http.createServer(server);
    const httpsServer = https.createServer(httpsOptions, server);

    httpServer.listen(3001, () => console.log('http://localhost:3001'));
    httpsServer.listen(3002, () => console.log('https://localhost:3002'));
}

export default server;