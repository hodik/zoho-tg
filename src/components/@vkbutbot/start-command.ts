import TelegramBot = require('node-telegram-bot-api');
import { Message } from 'node-telegram-bot-api';
import { BOT_COMMANDS } from './commands';

export async function startHandler(bot: TelegramBot, msg: Message): Promise<void> {
    const sortedCommandList = BOT_COMMANDS.sort((a, b) => a.command.localeCompare(b.command));
    const descriptionList = sortedCommandList.map((command) => `${command.command} - ${command.description}${command.example ? `( Пример: ${command.example})` : ''}`);
    bot.sendMessage(msg.chat.id, descriptionList.join('\n'));
} 