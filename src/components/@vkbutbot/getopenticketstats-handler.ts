import TelegramBot = require('node-telegram-bot-api');
import { Message } from 'node-telegram-bot-api';
import { getOpenTicketsStats } from '../zoho/zoho-stats';

const ZOHO_AUTH_TOKEN: string = process.env.ZOHO_AUTH_TOKEN;
const ZOHO_DESK_ORG_ID: string = process.env.ZOHO_DESK_ORG_ID;

export async function getOpenTicketStatsHandler(bot: TelegramBot, msg: Message): Promise<void> {
    try {
        const stats = await getOpenTicketsStats(ZOHO_AUTH_TOKEN, ZOHO_DESK_ORG_ID);

        const response: string[] = ['Разбивка по количеству тикетов в разных состояниях у агентов:'];
        Object.keys(stats).forEach((email) => {
            const ticketsByStatus = stats[email];
            
            response.push('---------------');
            response.push(`Агент ${email}`);
            response.push('---------------');
            Object
                .keys(ticketsByStatus)
                .forEach((status) => response.push(`${status} ${ticketsByStatus[status]}`))
            
        });

        const extraParams = msg.message_id ?  {
            reply_to_message_id: msg.message_id
        } : undefined
        
        await bot.sendMessage(msg.chat.id, response.join('\n'), extraParams);
    } catch(err) {
        bot.sendMessage(msg.chat.id, `Произошла ошибка: ${err.message}`, {
            reply_to_message_id: msg.message_id
        }); 
        console.error(err);
    }
}