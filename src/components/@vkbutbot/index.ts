import { findCommandHandler } from './commands';
import {Express, Request, Response} from 'express-serve-static-core';
import TelegramBot = require('node-telegram-bot-api');
import * as asyncHandler from 'express-async-handler';

export const API_KEY: string = process.env.VKBUTBOT_API_KEY;
export const BOT_SINGLETON = new TelegramBot(API_KEY);
const ALLOWED_CHATS = (process.env.VKBUTBOT_ALLOWED_CHATS || '').split(',');

async function botHandler(request: Request, response: Response) {
    const message = <TelegramBot.Message>request.body.message;

    try {
        console.log(`ALLOWED_CHATS = ${JSON.stringify(ALLOWED_CHATS)}`);
        console.log(`message.chat.id.toString() = ${message.chat.id.toString()}`);
        console.log(`message.chat.username = ${message.chat.username}`);
        
        if(!ALLOWED_CHATS.includes(message.chat.id.toString()) && !ALLOWED_CHATS.includes(message.chat.username)) {
            throw new Error('You are not allowed to use this bot');
        }

        const handler = findCommandHandler(message.text);
        if(!handler) {
            throw new Error('Unknown command');
        }

        await handler(BOT_SINGLETON, message);
    } catch(error) {
        BOT_SINGLETON.sendMessage(message.chat.id, error.message, {
            reply_to_message_id: message.message_id
        });
    }
    finally {
        response.sendStatus(200);
    }
}

export async function getWebhookUrl(): Promise<string> {
    return (await BOT_SINGLETON.getWebHookInfo()).url;
}

export async function setWebHook(host: string): Promise<void> {
    return await BOT_SINGLETON.setWebHook(`https://${host}/bot${API_KEY}`);
}

export function addWebhookRouteToExpress(server: Express) {
    server.post(`/bot${API_KEY}`, asyncHandler(botHandler));
}
