import TelegramBot = require('node-telegram-bot-api');

export type BotMessageHandler = (bot: TelegramBot, message: TelegramBot.Message) => void;
export interface IBotCommand {
    command: string;
    handler: BotMessageHandler;
    description: string;
    example?: string;
};