import { IBotCommand, BotMessageHandler } from "./interfaces";
import { getOpenTicketStatsHandler } from "./getopenticketstats-handler";
import { startHandler } from "./start-command";

export const BOT_COMMANDS: IBotCommand[] = [{
    command: '/getopenticketstats',
    description: 'Разбивка тикетов по статусам и агентам из техподдержки',
    handler: getOpenTicketStatsHandler    
}, {
    command: '/start',
    description: 'Список возможных команд',
    handler: startHandler

}];

export function findCommandHandler(message: string): BotMessageHandler {
    const firstToken = message.split(' ')[0];
    const command: IBotCommand = BOT_COMMANDS.find((command) => {
        const regex = new RegExp(`^${command.command}(@[a-zA-Z0-9]+)?`);
        return firstToken.match(regex) !== null;
    });

    if(!command) {
        return;
    }

    return command.handler;
}