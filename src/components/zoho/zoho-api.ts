import axios from 'axios';
import {stringify} from 'querystring';

export interface ITicketOptions {
    include?: string;
    status?: string;
    receivedInDays?: 15 | 30 | 90;
    limit?: number;
    from?: number;
}

export interface ITicket {
    assigneeId: string;
    createdTime: string;
    status: string;
    statusType: 'Open' | 'Closed';
    assignee?: {
        email: string;
    }
}

export interface IZohoDeskResponse<T> {
    data: T;
}

export async function getTickets(authToken: string, orgId: string, options?: ITicketOptions): Promise<ITicket[]> {
    const query = options ? stringify(options) : '';
    const url = `https://desk.zoho.com/api/v1/tickets?${query}`;

    const response = await axios.get<IZohoDeskResponse<ITicket[]>>(url, {
        headers: {
            orgId,
            Authorization: `Zoho-authtoken ${authToken}`
        }, 
        timeout: 5000
    });

    return response.data.data;
}