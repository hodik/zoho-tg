import { getTickets, ITicket } from "./zoho-api";
export type ITicketStatusesByAgent = {[id: string]: {[id: string]: number}};

export async function getOpenTicketsStats(authToken: string, orgId: string): Promise<ITicketStatusesByAgent> {
    let allTickets: ITicket[] = [];
    let ticketsFromZoho: ITicket[] = [];
    let fromNumber: number = 0;
    do {
        allTickets = [...allTickets, ...ticketsFromZoho];
        
        fromNumber += ticketsFromZoho.length;
        ticketsFromZoho = await getTickets(authToken, orgId, {
            include: 'assignee', 
            receivedInDays: 90,
            limit: 90,
            from: fromNumber
        });
    } while(ticketsFromZoho && ticketsFromZoho.length > 0)


    const ticketStatusesByAgent: {[id: string]: {[id: string]: number}} = {};
    allTickets
        .filter((ticket) => ticket.statusType === 'Open')
        .forEach((ticket) => {
            const agentId = ticket.assignee.email;
            
            let ticketStatuses = ticketStatusesByAgent[agentId];
            if(!ticketStatuses) {
                ticketStatuses = ticketStatusesByAgent[agentId] = {}
            }

            const statusCount = ticketStatuses[ticket.status] || 0;
            ticketStatuses[ticket.status] = statusCount + 1;
        });

    return ticketStatusesByAgent;
}