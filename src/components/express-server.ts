import * as express from 'express';
import {Express} from 'express-serve-static-core';
import * as bodyParser from 'body-parser';

export function createServer(): Express {
    const app = express();
    app.use(bodyParser.json());
    return app;
}