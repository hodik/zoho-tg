import { setUpTelegramBot } from './@vkbutbot';
import {Express} from 'express-serve-static-core';

export default function(server: Express) {
    setUpTelegramBot(server);
}