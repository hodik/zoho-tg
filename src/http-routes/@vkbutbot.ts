import {Express, Request, Response} from 'express-serve-static-core';
import {URL} from "url";
import { getWebhookUrl, setWebHook, addWebhookRouteToExpress, BOT_SINGLETON  } from '../components/@vkbutbot';
import { getOpenTicketStatsHandler } from '../components/@vkbutbot/getopenticketstats-handler';
import * as asyncHandler from 'express-async-handler';

function getHost(urlString: string): string {
    const url = new URL(urlString);
    return `${url.protocol}://${url.host}`;
}

async function overrideWebhook(request: Request, response: Response) {
    const oldWebhookUrl = await getWebhookUrl();
    await setWebHook(request.headers.host);

    response.json({
        oldWebhookUrlHost: getHost(oldWebhookUrl),
        newWebhookUrlHost: `https://${request.headers.host}`
    });
}

async function cronHandler(request: Request, response: Response) {
    await getOpenTicketStatsHandler(BOT_SINGLETON, {
        message_id: 0,
        date: new Date().getTime(),
        chat: {
            // @tholhaha
            id: 134653854,
            type: 'private',

        }
    });

    response.json({status: 'OK'});
}

export function setUpTelegramBot(server: Express) {
    addWebhookRouteToExpress(server);
    server.get('/bot/init', asyncHandler(overrideWebhook));
    server.get('/bot/cron', asyncHandler(cronHandler));
}